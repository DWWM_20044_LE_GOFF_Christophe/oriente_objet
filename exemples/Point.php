<?php

class Point
{
	//Les attributs
	public const VALEUR_PI = 3.14;
	public ?int $x = 0; // abscisse du point
	public ?int $y = 0; // ordonnée du point

	//Les méthodes
	// translate de point de (dx,dy)
	public function translate(int $dx, int $dy): void
	{
		echo "toto";
		$this->x = $this->x + $dx;
		$this->y = $this->y + $dy;
	}
	// calcule la distance du point à l’origine
	//regle generale d(A,B)=√(x2−x1)2+(y2−y1)2.
	public function distanceOrigine(): float
	{
		return sqrt($this->x * $this->x + $this->y * $this->y);
	}

	// calcule la distance du point à l’origine
	//regle generale d(A,B)=√(x2−x1)2+(y2−y1)2.
	public function distance(Point $otherPoint): float
	{
		return sqrt(pow($otherPoint->x - $this->x, 2) + pow($otherPoint->y - $this->y, 2));
	}


	public function afficher(): void
	{
		echo ("(" . $this->x . ", " . $this->y . ")" . PHP_EOL);
	}

	public function __construct(?int $dx=null,?int $dy=null)
	{

		$this->x = $dx;
		$this->y = $dy;
		 
	}
}
