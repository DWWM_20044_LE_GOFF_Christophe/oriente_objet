<?php
require ("Point.php");//importer la classe point 

//création d'un objet ou une instance 
$point1 = new Point();
$point1 = new Point(2);
$point1 = new Point(0,5);
$point1 = new Point(2,5);

echo (Point::VALEUR_PI);
//Point::VALEUR_PI=34;
$point1->x = 2;
$point1->y = 5;

$point1->afficher();

echo "la distance du point1 par rapport à l’origine est : " . $point1->distanceOrigine().PHP_EOL;

$point1->translate(4,4);

echo "la distance du point1 par rapport à l’origine après une stranslation de (4,4) est : " . $point1->distanceOrigine().PHP_EOL;

$point2 = new Point();
$point2->x = 22;
$point2->y = 98;
$point2->afficher();
echo "la distance du point2 par rapport à l’origine est : " . $point2->distanceOrigine().PHP_EOL;

echo "La distance entre le point1 et point2 est : " . $point1->distance($point2).PHP_EOL;

echo "La distance entre le point2 et point1 est : " . $point2->distance($point1).PHP_EOL;

$point2=null;
unset($point2);



?>