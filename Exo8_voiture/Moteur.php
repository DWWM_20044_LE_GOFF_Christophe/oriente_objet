<?php

class Moteur
{
    // attributs
    private int $nbreCilyndre;
    private int $puissance;

    public function __construct(int $nbreCilyndre, int $puissance)
    {
        $this->nbreCilyndre = $nbreCilyndre;
    }

    /**
     * getNbreCilyndre
     *
     * @return int
     */
    public function getNbreCilyndre(): int
    {
        return $this->nbreCilyndre;
    }

    /**
     * setNbreCilyndre
     *
     * @param  mixed $nbreCilyndre
     * @return void
     */
    public function setNbreCilyndre($nbreCilyndre)
    {
        $this->nbreCilyndre = $nbreCilyndre;
    }

    public function getPuissance()
    {
        return $this->puissance;
    }

    public function setPuissance($puissance) {
        $this->puissance = $puissance;
    }
}
