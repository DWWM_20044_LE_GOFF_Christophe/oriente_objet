<?php

class Cilyndre
{
    // attributs
    private int $nbreCilyndre;

    public function __construct(int $nbreCilyndre)
    {
        $this->nbreCilyndre = $nbreCilyndre;
    }
    
    /**
     * getNbreCilyndre
     *
     * @return int
     */
    public function getNbreCilyndre(): int
    {
        return $this->nbreCilyndre;
    }
    
    /**
     * setNbreCilyndre
     *
     * @param  mixed $nbreCilyndre
     * @return void
     */
    public function setNbreCilyndre($nbreCilyndre)
    {
        $this->nbreCilyndre = $nbreCilyndre;
    }
}
