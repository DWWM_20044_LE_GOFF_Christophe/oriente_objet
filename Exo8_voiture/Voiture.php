<?php

class Voiture
{
    // attributs
    private int $nbrePortieres;
    private string $moteur;
    private int $nbreRoue;

    // methodes
    public function __construct(int $nbrePortieres, string $moteur, int $nbreRoue)
    {
        $this->$nbrePortieres;
        $this->$moteur;
        $this->$nbreRoue;
    }
    
    /**
     * getNbrePortieres
     *
     * @return int
     */
    public function getNbrePortieres(): int
    {
        return $this->nbrePortieres;
    }
    
    /**
     * setNbrePortieres
     *
     * @param  mixed $nbrePortieres
     * @return void
     */
    public function setNbrePortieres($nbrePortieres)
    {
        $this->nbrePortieres = $nbrePortieres;
    }
    
    /**
     * getMoteur
     *
     * @return string
     */
    public function getMoteur(): string
    {
        return $this->moteur;
    }
    
    /**
     * setMoteur
     *
     * @param  mixed $moteur
     * @return void
     */
    public function setMoteur($moteur)
    {
        $this->moteur = $moteur;
    }
    
    /**
     * getNbreRoue
     *
     * @return int
     */
    public function getNbreRoue(): int
    {
        return $this->nbreRoue;
    }
    
    /**
     * setNbreRoue
     *
     * @param  mixed $nbreRoue
     * @return void
     */
    public function setNbreRoue($nbreRoue)
    {
        $this->nbreRoue = $nbreRoue;
    }
}
