<?php

class Portiere
{
    // attributs
    private int $nbrePortiere;

    public function __construct(int $nbrePortiere)
    {
        $this->nbrePortiere = $nbrePortiere;
    }
    
    /**
     * getnbrePortiere
     *
     * @return int
     */
    public function getnbrePortiere(): int
    {
        return $this->nbrePortiere;
    }
    
    /**
     * setnbrePortiere
     *
     * @param  mixed $nbrePortiere
     * @return void
     */
    public function setnbrePortiere($nbrePortiere)
    {
        $this->nbrePortiere = $nbrePortiere;
    }
}
