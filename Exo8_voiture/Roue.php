<?php

class Roue
{
    // attributs
    private int $nbreRoue;

    public function __construct(int $nbreRoue)
    {
        $this->nbreRoue = $nbreRoue;
    }

    /**
     * getnbreRoue
     *
     * @return int
     */
    public function getNbreRoue(): int
    {
        return $this->nbreRoue;
    }

    /**
     * setnbreRoue
     *
     * @param  mixed $nbreRoue
     * @return void
     */
    public function setNbreRoue($nbreRoue)
    {
        $this->nbreRoue = $nbreRoue;
    }

}
