<?php

class Pneu
{
    // attributs
    private int $diametre;
    private int $largeur;

    public function __construct(int $diametre, int $largeur)
    {
        $this->diametre = $diametre;
    }

    /**
     * getdiametre
     *
     * @return int
     */
    public function getDiametre(): int
    {
        return $this->diametre;
    }

    /**
     * setdiametre
     *
     * @param  mixed $diametre
     * @return void
     */
    public function setDiametre($diametre)
    {
        $this->diametre = $diametre;
    }

    public function getLargeur()
    {
        return $this->largeur;
    }

    public function setLargeur($largeur) {
        $this->largeur = $largeur;
    }
}
