<?php

namespace gestionEleves;

class Eleve
{
    //attributs
    private string $nom;
    private array $listeNotes;
    private ?float $moyenne;
    private int $note;

    // méthodes
    public function __construct(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * getNom
     *
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * getListeNotes
     *
     * @return array
     */
    public function getListeNotes()
    {
        return $this->listeNotes;
    }

    /**
     * Set the value of listeNotes
     *
     * @return  self
     */
    public function setListeNotes(array $listeNotes)
    {
        $this->listeNotes = $listeNotes;
    }

    /**
     * getMoyenne
     *
     * @return int
     */
    public function getMoyenne(): int
    {
        return $this->moyenne;
    }

    /**
     * Set the value of moyenne
     *
     * @return  self
     */
    public function setMoyenne(float $moyenne)
    {
        $this->moyenne = $moyenne;
    }

    /**
     * ajouterNote
     *
     * @param  mixed $note
     * @return void
     */
    public function ajouterNote(int $note, array &$listeNotes, ?int &$moyenne=0): void
    {
        if ($note < 0) {
           $listeNotes[] = 0;
        } elseif ($note > 20) {
           $listeNotes[] = 20;
        } else {
           $listeNotes[] = $note;
        }
        if (!empty($listeNotes)) {
            foreach ($listeNotes as $notes) {
                $moyenne = $moyenne + $notes;
            }
            $this->moyenne = number_format($moyenne, 2) / count($listeNotes);
        } else {
            $this->moyenne = null;
        }
    }

    public function __toString()
    {
        return ($this->nom . " (" . number_format($this->moyenne, 2) . ")" . PHP_EOL);
    }
 
    /**
     * getNote
     *
     * @return int
     */
    public function getNote() : int
    {
        return $this->note;
    }

    /**
     * Set the value of note
     *
     * @return  self
     */ 
    public function setNote($note)
    {
        $this->note = $note;
    }
}
 