<?php

namespace afpa\Test;
require_once "afpa/Professeur.php";
require_once "afpa/Specialite.php";
require_once "ensam/Professeur.php";

$cptId = 1;
use ArrayObject;
use fr\afpa as afpa;
use fr\afpa\Specialite;
use fr\ensam as ensam;

$p1 = new afpa\Professeur($cptId++, "SAFI", "Amal", "0606060606", "safi@gmail.com");
$p2 = new ensam\Professeur($cptId++, "SAID", "alami", "0707070707", "safi@gmail.com");
$tab = new ArrayObject([new afpa\Specialite(),new afpa\Specialite(), new afpa\Specialite()]);

$p1->setSpecialites($tab);
$p1->getSpecialites()[] = new Specialite(1, "JAVA/JEE");

$p1->addSpecialites(new afpa\Specialite());

