<?php

require_once("afpa/Professeur.php");
require_once("afpa/Specialite.php");
require_once("afpa/Test.php");

$cptId = 0;
use fr\afpa as afpa;

$p1 = new afpa\Professeur(++$cptId, "SAFI", "Amal", "0606060606", "safi@gmail.com");
$p2 = new afpa\Professeur(++$cptId, "SAID", "alami", "0707070707", "safi@gmail.com");

$p1->addSpecialites(new afpa\Specialite(1, "JAVA/JEE"));
$p2->addSpecialites(new afpa\Specialite(2, "JAVA/JEE"));


if ($cptId === 0) {
    echo("Aucun professeur trouvé ");
} else {
    echo("professeurs par spécialités : " . PHP_EOL);
echo($p1->getSpecialites()[0] . PHP_EOL);
echo($p1 . PHP_EOL);
echo($p2->getSpecialites()[0] . PHP_EOL);
echo($p2 . PHP_EOL);
}




