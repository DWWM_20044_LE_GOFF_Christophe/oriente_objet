<?php

namespace fr\afpa;

use ArrayObject;

/**
 * Professeur
 */
class Professeur
{
    //attributs
    private ?int $id;
    private string $nom;
    private string $prenom;
    private string $telephone;
    private string $email;
    private string $code;
    private ?ArrayObject $specialites;

    //methodes

    /**
     * getId
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * getNom
     *
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * getPrenom
     *
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @return  self
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * getTelephone
     *
     * @return int
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * Set the value of telephone
     *
     * @return  self
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * getEmail
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function __construct(?int $id = null, string $nom = "", string $prenom = "", string $telephone = "", string $email = "", ?Array $specialites = null)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->telephone = $telephone;
        $this->email = $email;
        if ($specialites === null)
            $this->specialites = new ArrayObject([]);
        else
            $this->specialites = $specialites;
    }

    public function __toString()
    {
        return ("                                     -" . $this->nom . " " . $this->prenom . " " . $this->email);
    }

    /**
     * getCode
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */
    public function setCode($code)
    {
        $this->code = $this->Test->getCode;
    }

    /**
     * Get the value of specialites
     */
    public function getSpecialites()
    {
        return $this->specialites;
    }

    /**
     * Set the value of specialites
     *
     * @param   mixed  $specialites  
     *
     * @return  self
     */
    public function setSpecialites($specialites)
    {
        $this->specialites = $specialites;

        //hello i'm momo
    }
    public function addSpecialites(Specialite $specialite)
    {
        $this->specialites[] = $specialite;
    }
}
