<?php

namespace fr\afpa;

use fr\afpa as name1;

class Test
{
    //attributs
    private ?int $id;
    private ?string $code;
    private string $libelle;

    //methodes
         
    /**
     * getCode
     *
     * @return string
     */
    public function getCode() : string
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */ 
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * getLibelle
     *
     * @return string
     */
    public function getLibelle() : string
    {
        return $this->libelle;
    }

    /**
     * Set the value of libelle
     *
     * @return  self 
     */ 
    public function setLibelle($libelle)
    {
        // for ($i=1; $i <= 5; $i++) {
        //     switch($this->Professeur->getCode()) {
        //         case 1: 
        //             return $this->libelle = "JAVA/JEE";
        //         break;
        //         case 2: 
        //             return $this->libelle = ".net";
        //         break;
        //         case 3: 
        //             return $this->libelle = "Gestion de projet";
        //         break;
        //         case 4: 
        //             return $this->libelle = "CISCO";
        //         break;
        //         case 5: 
        //             return $this->libelle = "PHP";
        //         break;
        //     }
        // }
        $this->libelle = $libelle;
    }

    public function __construct(?int $id=null, ?string $code=null)
    {
        $this->id = $id;
        $this->code = $code;
    }

    public function __toString()
    {
        return ("Spécialité " . $this->libelle);
    }
     
    /**
     * getId
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }
}


