<?php

namespace fr\afpa;

class Specialite
{
    //attributs
    private ?int $id;
    private string $code;
    private string $libelle;

    //methodes

    /**
     * getId
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     * getCode
     *
     * @return string
     */
    public function getCode() : string
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */ 
    public function setCode($code)
    {
        $this->code = $code;
    }
    
    /**
     * getLibelle
     *
     * @return string
     */
    public function getLibelle() : string
    {
        return $this->libelle;
    }

    /**
     * Set the value of libelle
     *
     * @return  self
     */ 
    public function setLibelle($libelle)
    {
        $this->libelle = $this->Test->getLibelle;
    }

    public function __construct(string $code="", string $libelle="")
    {
        $this->code = $code;
        $this->libelle = $libelle;
    }

    public function __toString()
    {
        return ("Spécialité " . $this->libelle);
    }
}
