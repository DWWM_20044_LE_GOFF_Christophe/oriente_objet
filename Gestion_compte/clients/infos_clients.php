<?php

while (true) {
    $cptCompte = 0;
    $cptClient = 0;

    $clientRecherche = readline("Saisir l'identifiant du client à afficher : ");
    foreach ($clients as $client) {
        if ($clientRecherche != $client["idClient"]) {
            $cptClient++;
        }
    }
    if ($cptClient == count($clients)) {
        echo ("Identifiant inconnu" . PHP_EOL);
        break;
    }

    while (true) {
        foreach ($comptes as $keys => $compte) {
            foreach ($compte as $key => $value) {
                if ($clientRecherche == $value) {
                    $idClient = $compte["idClient"];
                    $cptCompte++;
                }
            }
        }

        break;
    }

    if ($cptCompte == 0) {
        change_color("red");
        readline("Ce client ne posséde pas de compte");
        change_color("");
    }


    if (isset($idClient)) {
        while (true) {
            foreach ($clients as $cles => $client) {
                foreach ($client as $cle => $val) {
                    if ($cle == "idClient" && $val == $idClient) {
                        break 3;
                    }
                }
            }
        }
    }


    $afficherFicheClient = ("sauv/clients/Infosclient.txt");
    $f = fopen($afficherFicheClient, 'w');

    $ficheClient = ("                      Fiche client" . PHP_EOL . PHP_EOL .
        "Numéro client : " . $client["idClient"] . PHP_EOL .
        "Nom : " . $client["nom"] . PHP_EOL .
        "Prénom : " . $client["prenom"] . PHP_EOL .
        "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL .
        "Liste de compte" . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL .
        "Numéro de compte                  Solde" . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL . PHP_EOL);


    fwrite($f, $ficheClient);
    if (isset($idClient)) {
        while (true) {
            foreach ($comptes as $cles => $compte) {
                foreach ($compte as $cle => $val) {
                    if ($cle == "idClient" && $val == $idClient) {
                        if ($compte["solde"] >= 0 && $compte["solde"] != "") {
                            $courant = ("    " . $compte["numeroCompte"] . "                   " . $compte["solde"] . "                   :-)" . PHP_EOL);
                            fwrite($f, $courant);
                        } elseif ($compte["solde"] <= 0 && $compte["solde"] != "") {
                            $courant = ("    " . $compte["numeroCompte"] . "                   " . $compte["solde"] . "                   :-(" . PHP_EOL);
                            fwrite($f, $courant);
                        }
                        if ($compte["soldeLivretA"] >= 0 && $compte["soldeLivretA"] != "") {
                            $livretA = ("    " . $compte["numeroCompte"] . "                   " . $compte["soldeLivretA"] . "                   :-)" . PHP_EOL);
                            fwrite($f, $livretA);
                        } elseif ($compte["soldeLivretA"] <= 0 && $compte["soldeLivretA"] != "") {
                            $livretA = ("    " . $compte["numeroCompte"] . "                   " . $compte["soldeLivretA"] . "                   :-(" . PHP_EOL);
                            fwrite($f, $livretA);
                        } 
                        if ($compte["soldePel"] >= 0 && $compte["soldePel"] != "") {
                            $pel = ("    " . $compte["numeroCompte"] . "                   " . $compte["soldePel"] . "                   :-)" . PHP_EOL);
                            fwrite($f, $pel);
                        } elseif ($compte["soldePel"] <= 0 && $compte["soldePel"] != "") {
                            $pel = ("    " . $compte["numeroCompte"] . "                   " . $compte["soldePel"] . "                   :-(" . PHP_EOL);
                            fwrite($f, $pel);
                        }
                    }
                }
            }
            break;
        }

        fclose($f);
        change_color("green");
        echo ("Document imprimé dans " . $afficherFicheClient . PHP_EOL);
        change_color("");
        readline("Appuyer sur entrer pour continuer ...");
        break;
    }
}
