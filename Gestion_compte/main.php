<?php

include_once("lib/utils.php");
include_once("lib/function.php");
require_once("banque/Agence.php");
require_once("banque/Client.php");
require_once("banque/Compte.php");

use banque as banque;

$cptCompte = 0;

while (true) {

    // csv_to_array($filename = 'sauv/agences/agences.csv', $delimiter = ',', $agences);
    $agences = [];
    csv_to_array($filename = 'sauv/agences/agences.csv', $delimiter = ',', $agences);
    $clients = [];
    csv_to_array($filename = 'sauv/clients/clients.csv', $delimiter = ',', $clients);
    $comptes = [];
    csv_to_array($filename = 'sauv/comptes/comptes.csv', $delimiter = ',', $comptes);

    // Menu Principal pour le programme de gestion de comptes bancaires
    change_color("blue");
    echo (PHP_EOL . "Menu :" . PHP_EOL . PHP_EOL);
    change_color("");
    change_color("purple");
    echo ("   ---------------------- DWWM_20044 Banque ----------------------------" . PHP_EOL .
        "   1- Créer une agence" . PHP_EOL .
        "   2- créer un client" . PHP_EOL .
        "   3- Créer un compte bancaire" . PHP_EOL .
        "   4- Recherche de compte (numéro de compte)" . PHP_EOL .
        "   5- Recherche de client (Nom, Numéro de compte, identifiant de client)" . PHP_EOL .
        "   6- Afficher la liste des comptes d’un client (identifiant client)" . PHP_EOL .
        "   7- Imprimer les infos client (identifiant client)" . PHP_EOL .
        "   8- Simuler frais de gestion" . PHP_EOL .
        "   ---------------------------------------------------------------------" . PHP_EOL .
        "   9- Quitter le programme" . PHP_EOL .
        "   ---------------------------------------------------------------------" . PHP_EOL . PHP_EOL);
    change_color("");
    change_color("blue");
    $choixMenu = (int)(readline("Votre choix : "));
    change_color("");
    echo (PHP_EOL);
    while (true) {
        if (!is_numeric($choixMenu) || $choixMenu < 1 || $choixMenu > 9) {
            change_color("red");
            $choixMenu = (int)(readline("choix  invalide : "));
            change_color("");
        } else {
            break;
        }
    }

    if ($choixMenu == 9) {

        break;
    } elseif ($choixMenu == 1) {

        // Menu 1 : Créer une agence
        $reponse = 'y';

        while ($reponse != "N") {
            $agence = new banque\Agence();
            $agence->setCodeAgence($agence, $agences);
            $agence->setNomAgence($agence);
            $agence->verrifNomDoublon($agences, $reponse);
            if ($reponse == "N") {
                break;
            }
            $agence->setAdresse();
            $agence->verrifAdresseDoublon($agences, $reponse);
            if ($reponse == "N") {
                break;
            }
            $agences[] = $agence;
            arrayToCsv($filename = 'sauv/agences/agences.csv', $delimiter = ',', $agences, $header = array("codeAgence", "nomAgence", "adresse"));
            change_color("purple");
            echo ("L'agence' n° " . $agence->getcodeAgence() . " a bien été créée" . PHP_EOL);
            change_color("");
            readline("Appuyer sur entrer ...");
            break;
        }
    } elseif ($choixMenu == 2) {

        // Menu 2 : Créer un client
        $reponse = 'y';

        while ($reponse != "N") {
            $client = new banque\Client();
            $client->setidClient($client, $clients);
            $client->setNom();
            $client->verrifNomDoublon($clients, $reponse);
            if ($reponse == "N") {
                break;
            }
            $client->setPrenom();
            $client->setDateDeNaissance();
            $client->setEmail();
            $client->verrifEmailDoublon($clients);
            $clients[] = $client;
            arrayToCsv($filename = 'sauv/clients/clients.csv', $delimiter = ',', $clients, $header = array("idClient", "nom", "prenom", "dateDeNaissance", "email"));
            change_color("purple");
            echo ("Le client n° " . $client->getidClient() . " a bien été créé" . PHP_EOL);
            change_color("");
            readline("Appuyer sur entrer ...");
            break;
        }
    } elseif ($choixMenu == 3) {

        // Menu 3 : Créer un compte bancaire
        $reponse = 'y';

        while ($reponse != "N") {
            $compte = new banque\compte();
            if (empty($agences)) {
                change_color("red");
                echo ("Aucune agence créée !!" . PHP_EOL);
                change_color("");
                $reponse = strtoupper(readline("appuyer sur une touche pour revenir au menu et selectionner 1 "));
                break;
            }
            if (empty($clients)) {
                change_color("red");
                echo ("Aucun client créée !!" . PHP_EOL);
                change_color("");
                $reponse = strtoupper(readline("appuyer sur une touche pour revenir au menu et selectionner 2 "));
                break;
            }
            $compte->setCodeAgence($agences);
            $compte->setIdClient($clients);
            $compte->nbreCompte($comptes, $cptCompte, $courant, $pel, $livretA);
            while (true) {
                echo ("Veuillez sélectionner le type de compte : " . PHP_EOL . PHP_EOL .
                    "------------------------------" . PHP_EOL .
                    " 1. Compte courant" . PHP_EOL .
                    " 2. Livret A" . PHP_EOL .
                    " 3. Plan Epargne Logement" . PHP_EOL .
                    "------------------------------" . PHP_EOL .
                    " 4. Revenir au menu principal" . PHP_EOL . PHP_EOL);

                $choixCompte = (int)readline("Entrer votre choix : ");
                while (true) {
                    if (!is_numeric($choixCompte) || $choixCompte < 1 || $choixCompte > 4) {
                        change_color("red");
                        $choixCompte = (int)readline("Invalide : Entrer votre choix : ");
                        change_color("");
                    }
                    break;
                }

                if ($cptCompte >= 3) {
                    change_color("red");
                    $reponse = strtoupper(readline("Le client n° " . $compte->getIdClient() . " possède déjà 3 comptes, appuyer sur touche pour revenir au menu principal"));
                    change_color("");
                    break 2;
                }

                if ($choixCompte == 4) {

                    break;
                } elseif ($choixCompte == 1) {
                    if (isset($courant) && $courant) {
                        change_color("red");
                        $reponse = strtoupper(readline("Le client n° " . $compte->getIdClient() . " possède déjà un compte courant, appuyer sur une touche"));
                        change_color("");
                        break 2;
                    }
                    $compte->setType("Courant");
                    $reponse = strtoupper(readline("Découvert autorisé (O/n) : "));
                    while (true) {
                        if ($reponse != "O" && $reponse != "N") {
                            change_color("red");
                            $reponse = strtoupper(readline("invalide! : Découvert autorisé (O/n) : "));
                            change_color("");
                        } else {
                            break;
                        }
                    }

                    if ($reponse == "O") {
                        $compte->setDecouvert($decouvert = true);
                        $reponse = (float)(readline("montant du découvert (100 <--> 1000 euros) : "));
                        while (true) {
                            if (!is_numeric($reponse) || $reponse < 100 || $reponse > 1000) {
                                change_color("red");
                                $reponse = (float)(readline("invalide! : montant du découvert (100 <--> 1000 euros) : "));
                                change_color("");
                            } else {
                                break;
                            }
                        }
                        $compte->setDecouvertMontant($reponse);
                    } else {
                        $compte->setDecouvert($decouvert = false);
                        $compte->setDecouvertMontant(null);
                    }

                    $reponse = (float)(readline("solde du compte (max 10 000 euros) : "));
                    if ($compte->getDecouvert() == true) {
                        while (true) {
                            if ($reponse == 0 || !is_numeric($reponse) || $reponse < -$compte->getDecouvertMontant() || $reponse > 10000) {
                                change_color("red");
                                if ($reponse < -$compte->getDecouvertMontant()) {
                                    echo ("invalide! : découvert autorisé (" . $compte->getDecouvertMontant() . " euros) dépassé. " . PHP_EOL);
                                    $reponse = (float)(readline("solde du compte (max 10 000 euros) : "));
                                } elseif ($reponse > 10000) {
                                    echo ("invalide! : solde max (10 000 euros) : " . PHP_EOL);
                                    $reponse = (float)(readline("solde du compte (max 10 000 euros) : "));
                                } elseif ($reponse == 0) {
                                    echo ("invalide! saisir un montant : solde max (10 000 euros) : " . PHP_EOL);
                                    $reponse = (float)(readline("solde du compte (max 10 000 euros) : "));
                                } else {
                                }
                                change_color("");
                            }
                            break;
                        }

                        $compte->setSolde($reponse);
                    } elseif ($compte->getDecouvert() == false) {
                        while (true) {
                            if (!is_numeric($reponse) || $reponse < 0 || $reponse > 10000) {
                                change_color("red");
                                $reponse = (float)(readline("Découvert non autorisé pour ce compte! : montant max (10 000 euros)."));
                                change_color("");
                            } else {
                                break;
                            }
                        }
                        $compte->setSolde($reponse);
                    }
                    $comptes[] = $compte;
                    $reponse = readline("appuyer sur une touche pour continuer");
                    break;
                } elseif ($choixCompte == 2) {

                    if ($livretA) {
                        change_color("red");
                        $reponse = strtoupper(readline("Le client n° " . $compte->getIdClient() . " possède déjà un livret A, appuyer sur une touche"));
                        change_color("");
                        break 2;
                    }
                    $compte->setType("LivretA");
                    $reponse = (float)(readline("solde du livret A (max 10 000 euros) : "));
                    while (true) {
                        if (!is_numeric($reponse) || $reponse < 0 || $reponse > 10000) {
                            change_color("red");
                            if ($reponse < 0) {
                                $reponse = strtoupper(readline("invalide! : solde négatif impossible!" . PHP_EOL . "solde du livret A (max 10 000 euros) :  "));
                            } else {
                                $reponse = strtoupper(readline("invalide! : appuyer sur une touche : "));
                            }
                            change_color("");
                        } else {
                            break;
                        }
                    }
                    $compte->setSoldeLivretA($reponse);
                    $comptes[] = $compte;
                    $reponse = readline("appuyer sur une autre touche pour continuer");
                    break;
                } elseif ($choixCompte == 3) {

                    if ($pel) {
                        change_color("red");
                        $reponse = strtoupper(readline("Le client n° " . $compte->getIdClient() . " possède déjà un PEL, appuyer sur une touche"));
                        change_color("");
                        break 2;
                    }
                    $compte->setType("PEL");;
                    $reponse = (float)(readline("solde du PEL (max 10 000 euros) : "));
                    while (true) {
                        if (!is_numeric($reponse) || $reponse < 0 || $reponse > 10000) {
                            change_color("red");
                            if ($reponse < 0) {
                                $reponse = strtoupper(readline("invalide! : solde négatif impossible! solde du PEL (max 10 000 euros) : "));
                            } else {
                                $reponse = strtoupper(readline("invalide! : solde du PEL (max 10 000 euros) : "));
                            }
                            change_color("");
                        } else {
                            break;
                        }
                    }
                    $compte->setSoldePel($reponse);
                    $comptes[] = $compte;
                    $reponse = readline("appuyer sur une autre touche pour continuer");

                    break;
                }
            }

            $compte->setNumeroCompte($comptes);
            arrayToCsv($filename = 'sauv/comptes/comptes.csv', $delimiter = ',', $comptes, $header = array("numeroCompte", "codeAgence", "idClient", "type", "decouvert", "decouvertMontant", "solde", "soldeLivretA", "soldePel"));
            change_color("green");
            echo ("Le compte n° " . $compte->getNumeroCompte() . " a bien été créé" . PHP_EOL);
            change_color("");

            $comptes[] = $compte;

            readline("Appuyer sur entrer ...");
            $cptCompte = 0;
            break;
        }
    } elseif ($choixMenu == 4) {

        // Menu 4 : Recherche de compte (numéro de compte)
        include "comptes/rechercheCompte.php";
    } elseif ($choixMenu == 5) {

        // Menu 5 : Recherche de client (Nom, Numéro de compte, identifiant de client)
        include "clients/RechercheClient.php";
    } elseif ($choixMenu == 6) {

        // Menu 6 : Afficher la liste des comptes d’un client (identifiant client)
        include "clients/affichageClientID.php";
    } elseif ($choixMenu == 7) {

        // Menu 7 : 7-	Imprimer les infos client (identifiant client)
        include "clients/infos_clients.php";
    } elseif ($choixMenu == 8) {

        // Menu 7 : 7-	Imprimer les infos client (identifiant client)
        include "comptes/SimulationFraisGestionComptes.php";
    }
}
