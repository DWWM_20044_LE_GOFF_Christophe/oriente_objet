<?php

namespace banque;

class Compte
{
    //attributs
    private ?int $numeroCompte;
    private ?string $codeAgence;
    private ?string $idClient;
    private string $type;
    private bool $decouvert;
    private ?float $decouvertMontant;
    private ?float $solde;
    private ?float $soldeLivretA;
    private ?float $soldePel;

    // méthodes
    public function __construct(?int $numeroCompte = null, ?string $codeAgence = null, ?string $idClient = null, string $type = "", bool $decouvert = false, ?float $decouvertMontant = null, ?float $solde = null, ?float $soldeLivretA = null, ?float $soldePel = null)
    {
        $this->numeroCompte = $numeroCompte;
        $this->codeAgence = $codeAgence;
        $this->idClient = $idClient;
        $this->type = $type;
        $this->decouvert = $decouvert;
        $this->decouvertMontant = $decouvertMontant;
        $this->solde = $solde;
        $this->soldeLivretA = $soldeLivretA;
        $this->soldePel = $soldePel;
    }

    /**
     * getnumeroCompte
     *
     * @return int
     */
    public function getNumeroCompte(): int
    {
        return $this->numeroCompte;
    }

    /**
     * setnumeroCompte
     *
     * @param  mixed $numeroCompte
     * @return void
     */
    public function setNumeroCompte($comptes)
    {
        $numeroCompte = random_int(10000000000, 99999999999);
            foreach ($comptes as $elements) {
                foreach ($elements as $key => $value) {
                    if ($key == "nom") {
                        while ($numeroCompte == $value) {
                            $numeroCompte = random_int(10000000000, 99999999999);
                        }
                    }
                }
            }
        return $this->numeroCompte = $numeroCompte;
    }

    /**
     * getCodeAgence
     *
     * @return string
     */
    public function getCodeAgence(): string
    {
        return $this->codeAgence;
    }

    /**
     * setCodeAgence
     *
     * @param  mixed $codeAgence
     * @return void
     */
    public function setCodeAgence($agences)
    {
        $quelleAgence = readline("enter le code agence : ");
        while (true) {
            foreach ($agences as $agence) {
                foreach ($agence as $key => $value) {
                    if ($key == "codeAgence" && $value == $quelleAgence) {
                        $compte["codeAgence"] = $quelleAgence;
                        change_color("green");
                        echo ("Agence saisie" . PHP_EOL);
                        change_color("");
                        break 3;
                    }
                }
            }
            change_color("red");
            $quelleAgence = readline("Entrée invalide : enter le code agence choisi : ");
            change_color("");
        }
        $this->codeAgence = $quelleAgence;
    }

    /**
     * getIdClient
     *
     * @return string
     */
    public function getIdClient(): string
    {
        return $this->idClient;
    }

    /**
     * setIdClient
     *
     * @param  mixed $idClient
     * @return void
     */
    public function setIdClient($clients)
    {
        $quelClient = readline("enter le code client choisi : ");
        while (true) {
            foreach ($clients as $client) {
                foreach ($client as $key => $value) {
                    if ($key == "idClient" && $value == $quelClient) {
                        $compte["idClient"] = $quelClient;
                        change_color("green");
                        echo ("client saisi" . PHP_EOL);
                        change_color("");
                        break 3;
                    }
                }
            }
            change_color("red");
            $quelClient = readline("Entrée invalide : enter le code client choisi : ");
            change_color("");
        }
        $this->idClient = $quelClient;
    }

    /**
     * getType
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * setType
     *
     * @param  mixed $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * getSolde
     *
     * @return float
     */
    public function getSolde(): float
    {
        return $this->solde;
    }

    /**
     * setSolde
     *
     * @param  mixed $solde
     * @return void
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;
    }

    /**
     * getDecouvert
     *
     * @return bool
     */
    public function getDecouvert(): bool
    {
        return $this->decouvert;
    }

    /**
     * setDecouvert
     *
     * @param  mixed $decouvert
     * @return void
     */
    public function setDecouvert($decouvert)
    {
        $this->decouvert = $decouvert;
    }
    
        
    /**
     * nbreCompte
     *
     * @param  mixed $comptes
     * @param  mixed $cptCompte
     * @return void
     */
    public function nbreCompte($comptes, &$cptCompte, &$courant, &$pel, &$livretA)
    {
        $cptCompte = 0;
        $pel = false;
        $courant = false;
        $livretA = false;
        foreach ($comptes as $elements) {
            $elements = (array)$elements;
            if ($elements["idClient"] == $this->idClient) {
                if ($elements["type"] == "PEL") {
                    $pel = true;
                    $cptCompte++;
                } elseif ($elements["type"] == "Courant") {
                    $courant = true;
                    $cptCompte++;
                } elseif ($elements["type"] == "LivretA") {
                    $livretA = true;
                    $cptCompte++;
                }
            }
        }
    }

    
    /**
     * getDecouvertMontant
     *
     * @return int
     */
    public function getDecouvertMontant(): int
    {
        return $this->decouvertMontant;
    }

    
    /**
     * setDecouvertMontant
     *
     * @param  mixed $decouvertMontant
     * @return void
     */
    public function setDecouvertMontant($decouvertMontant)
    {
        $this->decouvertMontant = $decouvertMontant;
    }

    /**
     * getSoldeLivretA
     *
     * @return float
     */
    public function getSoldeLivretA(): float
    {
        return $this->soldeLivretA;
    }

    /**
     * setSoldeLivretA
     *
     * @param  mixed $soldeLivretA
     * @return void
     */
    public function setSoldeLivretA($soldeLivretA)
    {
        $this->soldeLivretA = $soldeLivretA;
    }

    /**
     * getSoldePel
     *
     * @return float
     */
    public function getSoldePel(): float
    {
        return $this->soldePel;
    }

    
    /**
     * setSoldePel
     *
     * @param  mixed $soldePel
     * @return void
     */
    public function setSoldePel($soldePel)
    {
        $this->soldePel = $soldePel;
    }
}
