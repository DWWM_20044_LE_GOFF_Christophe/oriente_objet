<?php

namespace banque;

include_once("lib/utils.php");

class Client
{
    // attributs
    private ?string $idClient;
    private string $nom;
    private string $prenom;
    private string $dateDeNaissance;
    private string $email;

    // methodes
    public function __construct(?string $idClient = null, string $nom = "", string $prenom = "", string $dateDeNaissance = "", string $email = "")
    {
        $this->idClient = $idClient;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->dateDeNaissance = $dateDeNaissance;
        $this->email = $email;
    }
    
    /**
     * getidClient
     *
     * @return int
     */
    public function getidClient(): string
    {
        return $this->idClient;
    }

    /**
     * setidClient
     *
     * @param  mixed $idClient
     * @return void
     */
    public function setidClient($idClient, $clients)
    {
        $idClient = strtoupper(substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2)) . random_int(100000, 999999);
        foreach ($clients as $elements) {
            foreach ($elements as $key => $value) {
                if ($key == "code") {
                    while ($idClient == $value) {
                        $idClient = strtoupper(substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2)) . random_int(100000, 999999);
                    }
                }
            }
        }
        $this->idClient = $idClient;
    }

    /**
     * getNom
     *
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * setNom
     *
     * @param  mixed $nom
     * @return void
     */
    public function setNom()
    {
        $nom = readline("Entrer le nom du client : ");

        while (!preg_match("/[a-zA-Z][\p{L}-]*$/", $nom)) {
            change_color("red");
            echo ("Nom invalide !" . PHP_EOL);
            change_color("");
            $nom = readline("Entrer le nom du client : ");
        }
        $this->nom = $nom;
    }

    public function verrifNomDoublon($clients, &$reponse)
    {
        $nom = $this->getNom();
        foreach ($clients as $elements) {
            $elements = (array)$elements;
            foreach ($elements as $key => $value) {
                if ($key == "nom") {
                    while ($nom == $value) {
                        change_color("red");
                        echo ("Une entrée proche a été trouvée : " . PHP_EOL . PHP_EOL);
                        change_color("");
                        change_color("purple");
                        echo ("    Client " . $elements["nom"] . PHP_EOL .
                            "    ID : " . $elements["idClient"] . PHP_EOL .
                            "    Mail : " . $elements["email"] . PHP_EOL . PHP_EOL);
                        change_color("");
                        $reponse = strtoupper(readline("Voulez-vous continuer (O)ui / (N)on ? "));
                        while ($reponse != "O" && $reponse != "N") {
                            change_color("red");
                            $reponse = strtoupper(readline("Réponse invalide, taper (O)ui / (N)on : "));
                            change_color("");
                        }
                        if ($reponse == "O") {
                            $this->nom = $nom;
                            return $this->nom;
                            break 3;
                        } else {
                            break 3;
                        }
                    }
                }
            }
        }
    }

    /**
     * getPrenom
     *
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * setPrenom
     *
     * @param  mixed $prenom
     * @return void
     */
    public function setPrenom()
    {
        $prenom = readline("Entrer le prenom : ");
        while (!preg_match("/[a-zA-Z][\p{L}-]*$/", $prenom)) {
            change_color("red");
            echo ("Prénom invalide !" . PHP_EOL);
            change_color("");
            $prenom = readline("Entrer le prénom du client : ");
        }
        $this->prenom = $prenom;
    }

    /**
     * getDateDeNaissance
     *
     * @return string
     */
    public function getDateDeNaissance(): string
    {
        return $this->dateDeNaissance;
    }

    /**
     * setDateDeNaissance
     *
     * @param  mixed $dateDeNaissance
     * @return void
     */
    public function setDateDeNaissance()
    {
        $jour = (int)readline("entrer le jour de naissance : ");
        change_color("red");
        while (!is_numeric($jour) || $jour < 1 || $jour > 31) {
            $jour = (int)readline("entrer 1 jour de naissance entre 1 et 31 : ");
        }
        change_color("");
        $mois = (int)readline("entrer le mois de naissance : ");
        change_color("red");
        while (!is_numeric($mois) || $mois < 1 || $mois > 12) {
            $mois = (int)readline("entrer 1 mois de naissance entre 1 et 12 : ");
        }
        change_color("");
        $annee = (int)readline("entrer l'année de naissance : ");
        change_color("red");
        while (!is_numeric($annee) || $annee < (date("Y") - 100) || $annee > date("Y") - 18) {
            $annee = (int)readline("entrer 1 année de naissance valide (+18ans) : ");
        }
        change_color("");

        while (!checkdate($mois, $jour, $annee) || $annee > 2002 || $annee < 1900) {
            change_color("red");
            echo ("Date de naissance invalide ! (+18ans)" . PHP_EOL);
            change_color("");
            $jour = (int)readline("entrer le jour de naissance : ");
            change_color("red");
            while (!is_numeric($jour) || $jour < 1 || $jour > 31) {
                $jour = (int)readline("entrer 1 jour de naissance entre 1 et 31 : ");
            }
            change_color("");
            $mois = (int)readline("entrer le mois de naissance : ");
            change_color("red");
            while (!is_numeric($mois) || $mois < 1 || $mois > 12) {
                $mois = (int)readline("entrer 1 mois de naissance entre 1 et 12 : ");
            }
            change_color("");
            $annee = (int)readline("entrer l'année de naissance : ");
            change_color("red");
            while (!is_numeric($annee) || $annee < (date("Y") - 100) || $annee > date("Y") - 18) {
                $annee = (int)readline("entrer 1 année de naissance valide (+18ans) : ");
            }
            change_color("");
        }

        $dateDeNaissance = $jour . "/" . $mois . "/" . $annee;
        $this->dateDeNaissance = $dateDeNaissance;
    }

    /**
     * getEmail
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * setEmail
     *
     * @param  mixed $email
     * @return void
     */
    public function setEmail()
    {
        $email = readline("Entrez l'email : ");
        while (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            change_color("red");
            $email = readline("Email invalide, Entrez l'email ( --- @ --- . com, fr ): ");
            change_color("");
        }
        $this->email = $email;
    }
    
    public function verrifEmailDoublon($clients) {
        $email = $this->getEmail();
        foreach ($clients as $client) {
            foreach ($client as $key => $value) {
                if ($key == "mail") {
                    while ($email == $value) {
                        change_color("red");
                        echo ("L'adresse email " . $email . " existe déjà : " . PHP_EOL . PHP_EOL);
                        change_color("");
                        change_color("purple");
                        echo ("    Client " . $this->nom . PHP_EOL .
                            "    ID : " . $this->code . PHP_EOL .
                            "    Mail : " . $this->mail . PHP_EOL . PHP_EOL);
                        change_color("");
                        $reponse = strtoupper(readline("Voulez-vous continuer ? (O)ui / (N)on) : "));
                        while ($reponse != "O" && $reponse != "N") {
                            change_color("red");
                            $reponse = strtoupper(readline("Réponse invalide, taper (O)ui / (N)on : "));
                            change_color("red");
                        }
                        if ($reponse == "O") {
                            $client["mail"] = $email;
                            break 3;
                        } elseif ($reponse == "N") {
                            break 3;
                        }
                    }
                }
            }
        }
        
    }
}
