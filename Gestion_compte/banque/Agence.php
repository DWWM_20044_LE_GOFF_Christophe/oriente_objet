<?php

namespace banque;

class Agence
{
    // attributs
    private ?string $codeAgence;
    private string $nomAgence;
    private string $adresse;

    // méthodes
    public function __construct(?string $codeAgence = null, string $nomAgence = "", string $adresse = "")
    {
        $this->codeAgence = $codeAgence;
        $this->nomAgence = $nomAgence;
        $this->adresse = $adresse;
    }

    /**
     * getcodeAgence
     *
     * @return string
     */
    public function getcodeAgence(): string
    {
        return $this->codeAgence;
    }

    /**
     * setcodeAgence
     *
     * @param  mixed $codeAgence
     * @return void
     */
    public function setcodeAgence($codeAgence, $agences)
    {
        $id = 1;
        $codeAgence = str_pad($id, 3, "0", STR_PAD_LEFT);
        foreach ($agences as $elements) {
            foreach ($elements as $key => $value) {
                if ($key == "codeAgence") {
                    while ($codeAgence == $value) {
                        $codeAgence = str_pad(++$id, 3, "0", STR_PAD_LEFT);
                    }
                }
            }
        }
        $this->codeAgence = $codeAgence;
    }

    /**
     * getNom
     *
     * @return string
     */
    public function getNomAgence(): string
    {
        return $this->nomAgence;
    }

    /**
     * setNom
     *
     * @param  mixed $nom
     * @return void
     */
    public function setNomAgence($nomAgence)
    {
        $nomAgence = readline("Entrer le nom de l'agence : ");
        while (!preg_match("/[a-zA-Z][\p{L}-]*$/", $nomAgence)) {
            change_color("red");
            echo ("Nom invalide !" . PHP_EOL);
            change_color("");
            $nomAgence = readline("Entrer le nom de l'agence : ");
        }
        $this->nomAgence = $nomAgence;
    }

    public function verrifNomDoublon($agences, &$reponse)
    {
        $nomAgence = $this->getNomAgence();
        foreach ($agences as $elements) {
            foreach ($elements as $key => $value) {
                if ($key == "nomAgence") {
                    while ($nomAgence == $value) {
                        change_color("red");
                        echo ("Une entrée proche a été trouvée : " . PHP_EOL . PHP_EOL);
                        change_color("");
                        change_color("purple");
                        echo ("    Agence " . $elements["nomAgence"] . PHP_EOL .
                            "    Code : " . $elements["codeAgence"] . PHP_EOL .
                            "    Adresse: " . $elements["adresse"] . PHP_EOL . PHP_EOL);
                        change_color("");
                        $reponse = strtoupper(readline("Voulez-vous continuer (O)ui / (N)on ? "));
                        while ($reponse != "O" && $reponse != "N") {
                            change_color("red");
                            $reponse = strtoupper(readline("Réponse invalide, taper (O)ui / (N)on : "));
                            change_color("");
                        }
                        if ($reponse == "O") {
                            $this->nomAgence = $nomAgence;
                            return $this->nomAgence;
                            break 3;
                        } else {
                            break 3;
                        }
                    }
                }
            }
        }
    }


    /**
     * getAdresse
     *
     * @return string
     */
    public function getAdresse(): string
    {
        return $this->adresse;
    }

    /**
     * setAdresse
     *
     * @param  mixed $adresse
     * @return void
     */
    public function setAdresse()
    {
        $numeroRue = readline("entrer le numéro de la rue, si pas de numéro taper \"N\" : ");
        while (!preg_match("~^[0-9]+$~", $numeroRue)) {
            if ($numeroRue != "N") {
                change_color("red");
                echo ("numéro de la rue invalide !" . PHP_EOL);
                change_color("");
                $numeroRue = readline("Entrer le numéro de la rue : ");
            } else {
                $numeroRue = "";
                break;
            }
        }
        $rue = readline("Renseigner la rue (obligatoire) : ");
        while ($rue == "" || is_numeric($rue)) {
            change_color("red");
            echo ("Rue ou lieu-dit (obligatoire!)" . PHP_EOL);
            change_color("");
            $rue = readline("Renseigner la rue : ");
        }
        $codePostal = readline("entrer le code postal (obligatoire) : ");
        while (!preg_match("~^[0-9]{5}$~", $codePostal)) {
            change_color("red");
            echo ("Code postal invalide !" . PHP_EOL);
            change_color("");
            $codePostal = readline("Entrer le code postal : ");
        }
        $ville = readline("entrer la ville : ");
        while ($ville == "" || is_numeric($ville)) {
            change_color("red");
            echo ("ville invalide!" . PHP_EOL);
            change_color("");
            $ville = readline("Entrer la ville : ");
        }
        $adresse = "" . $numeroRue . " " . $rue . " " . $codePostal . " " . $ville;
        $this->adresse = $adresse;
    }

    public function verrifAdresseDoublon($agences, &$reponse)
    {
        $adresse = $this->getAdresse();
        foreach ($agences as $elements) {
            foreach ($elements as $key => $value) {
                if ($key == "nom") {
                    while ($adresse == $value) {
                        change_color("red");
                        echo ("Une entrée proche a été trouvée : " . PHP_EOL . PHP_EOL);
                        change_color("");
                        change_color("purple");
                        echo ("    Agence " . $elements["nomAgence"] . PHP_EOL .
                            "    Code : " . $elements["codeAgence"] . PHP_EOL .
                            "    Adresse: " . $elements["adresse"] . PHP_EOL . PHP_EOL);
                        change_color("");
                        $reponse = strtoupper(readline("Voulez-vous continuer (O)ui / (N)on ? "));
                        while ($reponse != "O" && $reponse != "N") {
                            change_color("red");
                            $reponse = strtoupper(readline("Réponse invalide, taper (O)ui / (N)on : "));
                            change_color("");
                        }
                        if ($reponse == "O") {
                            $this->adresse = $adresse;
                            return $this->adresse;
                            break 3;
                        } else {
                            break 3;
                        }
                    }
                }
            }
        }
    }
}
