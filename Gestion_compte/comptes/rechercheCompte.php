<?php

$trouve = 0;
while (true) {

    if (!isset($comptes) || empty($comptes)) {
        change_color("red");
        echo ("Aucun compte existant!" . PHP_EOL);
        $reponse = strtoupper(readline("appuyer sur une touche pour revenir au menu et selectionner 3 "));
        change_color("");
        break;
    }
    echo (PHP_EOL);
    $compteRecherche = (int)readline("Saisir le numero de compte : ");
    while ($compteRecherche == "") {
        change_color(("red"));
        $compteRecherche = (int)readline("Invalide! Veuillez Saisir le numéro de compte du client à afficher : ");
        change_color("");
    }
    while (true) {
        foreach ($comptes as $keys => $compte) {
            foreach ($compte as $key => $value) {
                if ($compteRecherche == $value) {
                    $idClient = $compte["idClient"];
                    $trouve = 1;
                    break 3;
                }
            }
        }
        if ($trouve != 1) {
            change_color("red");
            readline("Aucun compte trouvé avec ce numéro de compte ! appuyer sur une touche pour continuer");
            change_color("");
            $trouve = 0;
            break;
        }
    }
    if (isset($idClient)) {
        while (true) {
            foreach ($clients as $cles => $client) {
                foreach ($client as $cle => $val) {
                    if ($cle == "idClient" && $val == $idClient) {
                        break 3;
                    }
                }
            }
        }
    } else {
        break;
    }
    change_color("blue");
    echo (PHP_EOL . PHP_EOL .
        "_______________________" . PHP_EOL .
        "Numéro client : " . $client["idClient"] . PHP_EOL .
        "Nom : " . $client["nom"] . PHP_EOL .
        "Prénom : " . $client["prenom"] . PHP_EOL .
        "Mail : " . $client["email"] . PHP_EOL .
        "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL .
        "_______________________" . PHP_EOL .
    "Compte recherché :" . PHP_EOL);
        
    change_color("");
    if (isset($idClient)) {
        $compteTrouve = 0;
        while (true) {
            foreach ($comptes as $cles => $compte) {
                foreach ($compte as $cle => $val) {
                    if ($cle == "idClient" && $val == $idClient) {

                        if ($compte["solde"] != "" && $compte["type"] == "Courant") {
                            change_color("green");
                            echo ("Compte courant numéro : " . $compte["numeroCompte"] . PHP_EOL . PHP_EOL);
                            change_color("");
                            $compteTrouve++;
                        }
                        if ($compte["soldeLivretA"] != "" && $compte["type"] == "LivretA"  && $compte["numeroCompte"] == $compteRecherche) {
                            change_color("green");
                            echo ("Livret A numéro : " . $compte["numeroCompte"] . PHP_EOL . PHP_EOL);
                            change_color("");
                            $compteTrouve++;
                        }
                        if ($compte["soldePel"] != "" && $compte["type"] == "PEL"  && $compte["numeroCompte"] == $compteRecherche) {
                            change_color("green");
                            echo ("Compte épargne logement numéro : " . $compte["numeroCompte"] . PHP_EOL . PHP_EOL);
                            change_color("");
                            $compteTrouve++;
                        }
                    }
                }
            }
            
            if ($compteTrouve == 0) {
                change_color("red");
                echo ("Aucun compte enregistré pour ce client" . PHP_EOL . PHP_EOL);
                change_color("");
                break;
            }
            break;
        }
        readline("Appuyer sur entrer");
        echo (PHP_EOL);
        break;
    }
    echo (PHP_EOL . PHP_EOL);
}
