<?php

$fraisTenueCompte = 25;
$trouve = 0;
while (true) {

    if (empty($comptes)) {
        change_color("red");
        echo ("Aucun compte existant!" . PHP_EOL);
        $reponse = strtoupper(readline("appuyer sur une touche pour revenir au menu et selectionner 3 "));
        change_color("");
        break;
    }
    echo(PHP_EOL);
    $compteRecherche = (int)readline("Saisir le numero de compte : ");
    while ($compteRecherche == "") {
        change_color(("red"));
        $compteRecherche = readline("Invalide! Veuillez Saisir le numéro de compte pour demarrer la simulation : ");
        change_color("");
    }
    while (true) {
        foreach ($comptes as $keys => $compte) {
            foreach ($compte as $key => $value) {
                if ($compteRecherche == $value) {
                    $idClient = $compte["idClient"];
                    $trouve = 1;
                    break 3;
                }
            }
        }
        if ($trouve != 1) {
            change_color("red");
            readline("Aucun compte trouvé avec ce numéro de compte ! appuyer sur une touvhe pour continuer");
            change_color("");
            $trouve = 0;
            break;
        }
    }

    if (isset($idClient)) {
        while (true) {
            foreach ($clients as $cles => $client) {
                foreach ($client as $cle => $val) {
                    if ($cle == "idClient" && $val == $idClient) {
                        break 3;
                    }
                }
            }
        }
    }

    $duree = (int)readline("Saisir le nombre d'années : ");
    while($duree == "" || !is_numeric($duree)) {
        change_color("red");
        $duree = (int)readline("Invalide! Saisir le nombre d'années : ");
        change_color("");
    }
    
    change_color("blue");
    echo (PHP_EOL . PHP_EOL .
        "Numéro client : " . $client["idClient"] . PHP_EOL .
        "Nom : " . $client["nom"] . PHP_EOL .
        "Prénom : " . $client["prenom"] . PHP_EOL .
        "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL . PHP_EOL);
    change_color("");

    if (isset($idClient)) {
        while (true) {
            foreach ($comptes as $cles => $compte) {
                foreach ($compte as $cle => $val) {
                    if ($cle == "idClient" && $val == $idClient) {
                        if ($compte["solde"] != "" && $compte["numeroCompte"] == $compteRecherche) {
                            change_color("green");
                            echo("Compte courant : " . $compte["numeroCompte"] . PHP_EOL);
                            echo (" solde actuel : " . (int)$compte["solde"] . " euros." .  PHP_EOL);
                            echo (" solde après " . $duree . " an(s) : " . ((int)$compte["solde"] - ($duree * $fraisTenueCompte)) . " euros." . PHP_EOL);
                            change_color("");
                        }
                        if ($compte["soldeLivretA"] != "" && $compte["numeroCompte"] == $compteRecherche) {
                            change_color("blue");
                            echo("Livret A : " . $compte["numeroCompte"] . PHP_EOL);
                            echo (" solde actuel : " . (int)$compte["soldeLivretA"] . " euros." .  PHP_EOL);
                            echo (" solde après " . $duree . " an(s) : " . ((int)$compte["soldeLivretA"] - ($duree * $fraisTenueCompte) - ($compte["soldeLivretA"] * 0.1)) . " euros." .  PHP_EOL);
                            change_color("");
                        }
                        if ($compte["soldePel"] != "" && $compte["numeroCompte"] == $compteRecherche) {
                            change_color("purple");
                            echo("Plan épargne logement : " . $compte["numeroCompte"] . PHP_EOL);
                            echo (" solde actuel : " . (int)$compte["soldePel"] . " euros." .  PHP_EOL);
                            echo (" solde après " . $duree . " an(s) : " . ((int)$compte["soldePel"] - ($duree * $fraisTenueCompte) - ($compte["soldePel"] * 0.025)) . " euros." .  PHP_EOL);
                            change_color("");
                        }
                    }
                }
            }
            break;
        }
        echo(PHP_EOL);
        readline("Appuyer sur entrer pour continuer ...");
        
        break;
    }
    echo(PHP_EOL);
}