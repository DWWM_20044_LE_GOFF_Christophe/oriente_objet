<?php

namespace fr\afpa;

class Etudiant
{
    // attributs
    private int $id;
    private string $nom;
    private string $prenom;
    private int $dateDeNaissance;
    
    // méthodes

    public function __construct(int $id, string $nom, string $prenom, int $dateDeNaissance)
    {
        
    }

    /**
     * getId
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param   mixed  $id  
     *
     * @return  self
     */
    public function setId($id){
        $this->id = $id;
    }
 
    /**
     * getNom
     *
     * @return string
     */
    public function getNom() : string
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @param   mixed  $nom  
     *
     * @return  self
     */
    public function setNom($nom){
        $this->nom = $nom;
    }
    
    /**
     * getPrenom
     *
     * @return string
     */
    public function getPrenom() : string
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @param   mixed  $prenom  
     *
     * @return  self
     */
    public function setPrenom($prenom){
        $this->prenom = $prenom;
    }
    
    /**
     * getDateDeNaissance
     *
     * @return int
     */
    public function getDateDeNaissance() : int
    {
        return $this->dateDeNaissance;
    }

    /**
     * Set the value of dateDeNaissance
     *
     * @param   mixed  $dateDeNaissance  
     *
     * @return  self
     */
    public function setDateDeNaissance($dateDeNaissance){
        $this->dateDeNaissance = $dateDeNaissance;
    }
}
