<?php
class Livre
{
    //Les attributs
    private ?int $id = 0;
    private ?string $titre;
    private ?string $auteurLivre;
    private ?float $prix = 1.0;

    public function __construct(?int $id = null, string $titre = null, ?string $auteur = null, ?float $prix = null)
    {
        $this->id = $id;
        $this->titre = $titre;
        $this->auteurLivre = $auteur;
        if ($prix != null)
            $this->prix = $prix;
    }

    //setters et getters
    public function getTitre(): ?string
    {
        return $this->titre;
    }
    public function setTitre(string $titre)
    {
        $this->titre = $titre;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        if ($id < 0)
            $id = 0;
        $this->id = $id;
    }

    public function getAuteurLivre(): ?string
    {
        return $this->auteurLivre;
    }
    public function setAuteurLivre(string $auteurLivre)
    {
        $this->auteurLivre = $auteurLivre;
    }
    public function getPrix(): ?float
    {
        return $this->prix;
    }
    public function setPrix(float $prix)
    {
        $this->prix = $prix;
    }



    //Méthodes
    public function lireLivre(?int $id = null, string $titre = null, ?string $auteur = null, ?float $prix = null)
    {
        $this->titre = readline("Donner le titre du livre n° $this->id :");
        $this->auteurLivre = readline("Donner l'auteur du livre n° $this->id");
        $this->prix = readline("Donner le prix du livre n° $this->id");
    }

    public function __toString()
    {
        return  "Le prix du livre " . $this->getTitre() . " de l'auteur " . $this->getAuteurLivre() . " est :" . $this->getPrix() . " DH \n";
    }
    public function __destruct()
    {
        echo "Le livre " . $this->getTitre() . " de l'auteur " . $this->getAuteurLivre() . " est  supprimé \n";
    }
}
