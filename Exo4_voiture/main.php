<?php

require_once("../Exo3_voiture/Voiture.php");

$cpt = 1;
$voitures = [];
$voiture = new Voiture($cpt++);
$voiture->setMarque("BMW");
$voiture->setVitesse(220.0);
$voiture->setPuissance(8);
$voitures[] = $voiture;
$voiture = new Voiture($cpt++,"Ford", 200.0, 6);
$voitures[] = $voiture;
$voiture = new Voiture($cpt++,"BMW", 240.0, 8);
$voitures[] = $voiture;
foreach ($voitures as $voiture) {
    echo($voiture . PHP_EOL);
}

