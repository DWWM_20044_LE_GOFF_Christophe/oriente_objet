<?php



class Voiture
{
    // attributs
    private ?int $id;
    private string $marque;
    private float $vitesse;
    private int $puissance;

     // méthodes

    /**
     * getId
     *
     * @return void
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * setId
     *
     * @param  mixed $id
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of marque
     */
    public function getMarque(): string
    {
        return $this->marque;
    }

    /**
     * setMarque
     *
     * @param  mixed $marque
     * @return void
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;
    }

    /**
     * getVitesse
     *
     * @return string
     */
    public function getVitesse(): string
    {
        return $this->vitesse;
    }

    /**
     * setVitesse
     *
     * @param  mixed $vitesse
     * @return void
     */
    public function setVitesse($vitesse)
    {
        $this->vitesse = $vitesse;
    }

    /**
     * getPuissance
     *
     * @return int
     */
    public function getPuissance(): int
    {
        return $this->puissance;
    }

    /**
     * setPuissance
     *
     * @param  mixed $puissance
     * @return void
     */
    public function setPuissance($puissance)
    {
        $this->puissance = $puissance;
    }
   
    public function __construct(?int $id = null, string $marque = "", float $vitesse = 0, int $puissance = 0)
    {
        $this->id = $id;
        $this->marque = $marque;
        $this->vitesse = $vitesse;
        $this->puissance = $puissance;
    }

    public function __toString()
    {
        return ("id=" . $this->id . ", marque=" . $this->marque . ", vitesse=" . $this->vitesse . ", puissance=" . $this->puissance);
    }
}
